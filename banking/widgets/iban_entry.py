from gi.repository import Gtk, GObject
from schwifty import IBAN


class IBANEntry(Gtk.Entry, Gtk.Editable):
    __gtype_name__ = 'IBANEntry'

    def __init__(self):
        super().__init__()

        self.__iban = None
        self.get_delegate().connect("insert-text", self._on_insert_text)
        self.get_delegate().connect("delete-text", self._on_delete_text)

    def _on_insert_text(self, entry, new_text, length, position):
        pos = entry.get_position()

        # Validate input
        for chr in new_text:
            if not chr.isalnum():
                entry.stop_emission_by_name('insert-text')
                return pos

        self.get_buffer().insert_text(pos, new_text, length)
        text = self.get_buffer().get_text()

        self.__iban = IBAN(text, allow_invalid=True)
        self.__validate_and_update_iban(text)

        new_pos = len(self.__iban.formatted)

        entry.handler_block_by_func(self._on_insert_text)
        entry.set_text(self.__iban.formatted)
        entry.handler_unblock_by_func(self._on_insert_text)

        GObject.idle_add(entry.set_position, new_pos)

        entry.stop_emission_by_name('insert-text')

        return new_pos

    def _on_delete_text(self, entry, start, end):
        text = self.get_text()

        self.__iban = IBAN(text, allow_invalid=True)
        self.__validate_and_update_iban(text)

    def __validate_and_update_iban(self, text):
        if self.__iban and self.__iban.is_valid:
            self.remove_css_class("error")
        else:
            self.add_css_class("error")

    @property
    def valid(self):
        return self.__iban and self.__iban.is_valid

    @property
    def iban(self):
        return self.__iban

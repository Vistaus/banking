# Copyright (c) 2020-2022 Jan-Michael Brummer <jan.brummer@tabos.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Transfer dialog
"""

from gi.repository import Gtk
from banking.widgets.iban_entry import IBANEntry # noqa
from schwifty import IBAN


@Gtk.Template(resource_path='/org/tabos/banking/ui/transfer.ui')
class Transfer(Gtk.Dialog):
    __gtype_name__ = 'Transfer'

    _name_entry = Gtk.Template.Child()
    _iban_entry = Gtk.Template.Child()
    _amount_entry = Gtk.Template.Child()
    _reason_view = Gtk.Template.Child()
    send_button = Gtk.Template.Child()
    contacts_liststore = Gtk.Template.Child()
    _account_stringlist = Gtk.Template.Child()
    _bank_label = Gtk.Template.Child()
    _account_comborow = Gtk.Template.Child()

    def __init__(self, backend, **kwargs):
        super().__init__(**kwargs)

        self._backend = backend

        self._amount_entry.connect('changed', self._on_amount_value_changed)
        self._iban_entry.get_delegate().connect('changed', self._on_iban_changed)

        self.__name_valid = False
        self.__amount_valid = False
        self.send_button.add_css_class("suggested-action")

        for account in self._backend.accounts:
            iban = IBAN(account.iban, allow_invalid=True)
            self._account_stringlist.append(iban.formatted)

        self.contacts_liststore.append(['Deutsche Bank', "DE89370400440532013000", "DEUTDEFFXXX"])

        self.__validate()

    def _on_iban_changed(self, entry):
        text = entry.get_text()
        iban = IBAN(text, allow_invalid=True)

        institute = None
        if iban.is_valid:
            institute = self._backend.find_institute(iban.bank_code)

        if institute:
            self._bank_label.set_text(institute['institute'])
        else:
            self._bank_label.set_text("")

    @Gtk.Template.Callback()
    def _on_name_entrycompletion_match_selected(self, widget, model, iter):
        name = model.get(iter, 0)
        iban_str = model.get(iter, 1)

        self._name_entry.set_text(name[0])
        self._iban_entry.set_text(iban_str[0])

        iban = IBAN(iban_str[0], allow_invalid=True)
        institute = self._backend.find_institute(iban.bank_code)
        if institute:
            self._bank_label.set_text(institute['institute'])
        else:
            self._bank_label.set_text("")

        self._amount_entry.grab_focus()

        self.__validate()

    @Gtk.Template.Callback()
    def _on_name_changed(self, entry):
        self.__name_valid = len(entry.get_text()) != 0
        self.__validate()

    def _on_amount_value_changed(self, entry):
        value = entry.get_value()

        self.__amount_valid = value != 0
        self.__validate()

    def __validate(self):
        valid = self._iban_entry.valid and self.__name_valid and self.__amount_valid
        self.send_button.set_sensitive(valid)

    @Gtk.Template.Callback()
    def _send_button_clicked(self, button):
        selected_account = self._account_comborow.get_selected()
        account = self._backend.accounts[selected_account]

        amount = self._amount_entry.get_value()
        iban = self._iban_entry.get_text().replace(' ', '')
        reason = self._reason_view.get_text()
        recipient = self._name_entry.get_text()
        account_name = account._account['owner_name']

        print(f"Sending from '{account_name}' '{amount}' to '{recipient}'/'{iban}', Reason '{reason}'")
        self.destroy()

        self._backend.sepa_transfer(account,
                                    account_name,
                                    recipient,
                                    self._iban_entry.iban,
                                    amount,
                                    reason)
